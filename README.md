# Компьютерные технологии в физике волновых процессов

## Coдержание

1.  [Знакомство с Matlab. Линейная алгебра](https://gitlab.com/aleksandragrigoreva/lections/computer-technologies/-/jobs/artifacts/master/file/presentations/1/document.pdf?job=build-run)
2.  [Функции](https://gitlab.com/aleksandragrigoreva/lections/computer-technologies/-/jobs/artifacts/master/file/presentations/2/document.pdf?job=build-run)
3.  [Системы нелинейных и дифференциальных уравнений](https://gitlab.com/aleksandragrigoreva/lections/computer-technologies/-/jobs/artifacts/master/file/presentations/3/document.pdf?job=build-run)
4.	[Нелинейные дифференциальные уравнения. Уравнения в частных производных](https://gitlab.com/aleksandragrigoreva/lections/computer-technologies/-/jobs/artifacts/master/file/presentations/4/document.pdf?job=build-run)
5.	[Преобразование Фурье](https://gitlab.com/aleksandragrigoreva/lections/computer-technologies/-/jobs/artifacts/master/file/presentations/5/document.pdf?job=build-run)
